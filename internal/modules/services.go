package modules

import (
	"gitlab.com/kuzmem/user/internal/infrastructure/component"
	uservice "gitlab.com/kuzmem/user/internal/modules/user/service"
	"gitlab.com/kuzmem/user/internal/storages"
)

type Services struct {
	User          uservice.Userer
	UserClientRPC uservice.Userer
}

func NewServices(storages *storages.Storages, components *component.Components) *Services {
	userService := uservice.NewUserService(storages.User, components.Logger)
	return &Services{
		User: userService,
	}
}
